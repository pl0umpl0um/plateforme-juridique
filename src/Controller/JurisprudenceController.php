<?php

namespace App\Controller;

use App\Entity\JurisprudenceCategory;
use App\Repository\JurisprudenceCategoryRepository;
use App\Repository\JurisprudenceRepository;
use App\Services\VisitorCountManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class JurisprudenceController extends AbstractController
{
    private JurisprudenceCategoryRepository $jurisprudenceCategoryRepository;
    private JurisprudenceRepository $jurisprudenceRepository;
    private VisitorCountManager $visitorCountManager;

    public function __construct(
        JurisprudenceCategoryRepository $jurisprudenceCategoryRepository,
        JurisprudenceRepository $jurisprudenceRepository,
        VisitorCountManager $visitorCountManager
    ) {
        $this->jurisprudenceCategoryRepository = $jurisprudenceCategoryRepository;
        $this->jurisprudenceRepository = $jurisprudenceRepository;
        $this->visitorCountManager = $visitorCountManager;
    }

    /**
     * @Route("/trouver-une-jurisprudence", name="jurisprudences_search")
     */
    public function jurisprudences(Request $request): Response
    {
        $jpData = [];
        $jurisprudencesAll = $this->jurisprudenceCategoryRepository->findBy([
            'enabled' => true
        ]);

        // Everything in php because the first version need too many SQL requests (14sec doctrine debug)
        /** @var JurisprudenceCategory $jurisprudenceCategory */
        foreach ($jurisprudencesAll as $jurisprudenceCategory) {
            if (null === $jurisprudenceCategory->getParent()) {
                $jpData[0][] = [
                    'id' => $jurisprudenceCategory->getId(),
                    'name' => $jurisprudenceCategory->getName(),
                    'enabled' => true,
                    'parentId' => null,
                    'createdAt' => $jurisprudenceCategory->getCreatedAt(),
                ];
            }
        }

        for ($i=1;$i<30;$i++) {
            /** @var JurisprudenceCategory $jurisprudenceCategory */
            foreach ($jurisprudencesAll as $jurisprudenceCategory) {

                // Category has no parent, stop iteration
                if (null === $jurisprudenceCategory->getParent()) {
                    continue;
                }

                // Previous iteration does not exist, stop the loop
                if (!array_key_exists($i-1, $jpData)) {
                    break;
                }

                if (array_search($jurisprudenceCategory->getParent()->getId(), array_column($jpData[$i-1], 'id')) !== false) {
                    $jpData[$i][] = [
                        'id' => $jurisprudenceCategory->getId(),
                        'name' => $jurisprudenceCategory->getName(),
                        'enabled' => true,
                        'parentId' => $jurisprudenceCategory->getParent()->getId(),
                        'createdAt' => $jurisprudenceCategory->getCreatedAt(),
                    ];
                }
            }
        }

        return $this->render('jurisprudence/default.html.twig', [
            'controller_name' => 'JurisprudenceController',
            'active_menu' => 'jp',
            'jpData' => $jpData,
        ]);
    }

    /**
     * @Route("/jurisprudences-liste", name="jurisprudences_list")
     */
    public function jurisprudencesList(Request $request): Response
    {
        if (!$request->query->has('categorie')) {
            return new Response();
        }

        $category = $this->jurisprudenceCategoryRepository->find((int) $request->query->get('categorie'));
        $jurisprudences = $this->jurisprudenceRepository->findBy([
            'active' => true,
            'category' => $category
        ], [
            'createdAt' => 'DESC'
        ]);

        $jurisprudencesClean = [];
        foreach ($jurisprudences as $key => $jurisprudence) {
            $content = $jurisprudence->getContent();
            $contentClean = strip_tags($content, '<br/><br>');
            $contentClean = str_replace("&rsquo;", "'", $contentClean);
            $contentClean = str_replace("&#39;", "'", $contentClean);
            $jurisprudence->setContent($contentClean);
            $jurisprudencesClean[] = $jurisprudence;
        }

        return $this->render('jurisprudence/list.html.twig', [
            'jurisprudences' => $jurisprudencesClean,
        ]);
    }

    /**
     * @Route("/jurisprudence/{jurisprudenceId}", name="jurisprudence_show")
     */
    public function jurisprudenceShow(string $jurisprudenceId): Response
    {
        $jurisprudence = $this->jurisprudenceRepository->find((int) $jurisprudenceId);
        if (null === $jurisprudence) {
            throw new NotFoundHttpException();
        }

        // Count visitors
        $this->visitorCountManager->incrementStats($jurisprudence);

        return $this->render('jurisprudence/show.html.twig', [
            'jurisprudence' => $jurisprudence,
            'active_menu' => 'jp',
        ]);
    }

    /**
     * @Route("/jurisprudences-mot-cle/{tag}", name="jurisprudences_by_tag")
     */
    public function jurisprudencesByTag(string $tag): Response
    {
        $jurisprudences = $this->jurisprudenceRepository->findJurisprudencesByTag($tag);

        return $this->render('jurisprudence/jurisprudences_by_tag.html.twig', [
            'jurisprudences' => $jurisprudences,
            'active_menu' => 'jp',
            'tag' => $tag
        ]);
    }
}
