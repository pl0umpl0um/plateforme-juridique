<?php

namespace App\Controller\Admin;

use App\Entity\Jurisprudence;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class JurisprudenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Jurisprudence::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('category')->setLabel('Catégorie'),
            TextField::new('name')->setLabel('Nom'),
            AssociationField::new('author')->hideOnIndex()->hideOnForm(),
            TextField::new('pdf')->hideOnForm(),
            TextareaField::new('pdfFile')->setFormType(VichImageType::class)->hideOnIndex(),
            TextField::new('content')
                ->setFormType(CKEditorType::class)
                ->setLabel('Contenu')
                ->hideOnIndex(),
            ArrayField::new('tags')->hideOnIndex()->setLabel('Mots clés'),
            BooleanField::new('active')->setLabel('Affichée'),
            ChoiceField::new('quality')->setLabel('Favorable')->hideOnIndex()->setChoices([
                'Favorable' => 'good',
                'Défavorable' => 'bad',
                "Ni l'un ni l'autre" => null
            ]),
            TextField::new('adress')->hideOnIndex()->setLabel('Adresse'),
            TextField::new('justiceCourt')->hideOnIndex()->setLabel('Cour de justice'),
            DateTimeField::new('date')->setLabel('Date de la jurisprudence'),
            DateTimeField::new('createdAt')->hideOnForm()->setLabel('Ajoutée le')
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_NEW, Action::INDEX);
        $actions->add(Crud::PAGE_EDIT, Action::INDEX);
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('category')
            ->add('name')
            ->add('tags')
            ->add('date')
            ;
    }
}
