<?php

namespace App\Controller;

use App\Repository\NewsRepository;
use App\Services\VisitorCountManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    private NewsRepository $newsRepository;
    private VisitorCountManager $visitorCountManager;

    public function __construct(NewsRepository $newsRepository, VisitorCountManager $visitorCountManager)
    {
        $this->newsRepository = $newsRepository;
        $this->visitorCountManager = $visitorCountManager;
    }

    /**
     * @Route("/lire-actu/{actuId}", name="news_read")
     */
    public function newsRead(string $actuId)
    {
        $news = $this->newsRepository->find((int) $actuId);
        if (null === $news) {
            throw new NotFoundHttpException();
        }
        
        // Count visitors
        $this->visitorCountManager->incrementStats($news);

        return $this->render('news/news_read.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'news' => $news
        ]);
    }
}
