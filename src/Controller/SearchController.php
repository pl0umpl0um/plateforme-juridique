<?php

namespace App\Controller;

use App\Entity\Jurisprudence;
use App\Entity\News;
use App\Entity\Tuto;
use App\Repository\JurisprudenceRepository;
use App\Repository\NewsRepository;
use App\Repository\TutoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private NewsRepository $newsRepository;
    private TutoRepository $tutoRepository;
    private JurisprudenceRepository $jurisprudenceRepository;

    public function __construct(
        NewsRepository $newsRepository,
        TutoRepository $tutoRepository,
        JurisprudenceRepository $jurisprudenceRepository
    )
    {
        $this->newsRepository = $newsRepository;
        $this->tutoRepository = $tutoRepository;
        $this->jurisprudenceRepository = $jurisprudenceRepository;
    }

    /**
     * @Route("/chercher", name="search")
     */
    public function search(Request $request)
    {
        if (!$request->query->has('search')) {
            return $this->redirectToRoute('homepage');
        }

        $data = [];
        $search = $request->query->get('search');
        $category = $request->query->get('categorie');
        if ('' !== $search) {
            if (null === $category || 'infos' === $category) {
                // Get news
                $news = $this->newsRepository->findNewsByTitle($search);
                /** @var News $new */
                foreach ($news as $new) {
                    // Dont add to data the new tutos info
                    if ($new->getCategory()->getName() === 'Tutos' || $new->getCategory()->getName() === 'tutos') {
                        continue;
                    }

                    $newsData = [
                        'title' => $new->getTitle(),
                        'content' => $this->cleanContent($new->getContent()),
                        'type' => 'news',
                        'id' => $new->getId()
                    ];

                    $data[$new->getCreatedAt()->getTimestamp()] = $newsData;
                }
            }

            if (null === $category || 'tutos' === $category) {
                // Get tutos
                $tutos = $this->tutoRepository->findTutosByTitle($search);
                /** @var Tuto $tuto */
                foreach ($tutos as $tuto) {
                    $tutoData = [
                        'title' => $tuto->getTitle(),
                        'content' => $this->cleanContent($tuto->getContent()),
                        'type' => 'tuto',
                        'id' => $tuto->getId()
                    ];

                    $data[$tuto->getCreatedAt()->getTimestamp()] = $tutoData;
                }
            }

            if (null === $category || 'jurisprudences' === $category) {
                // Get jp
                $jurisprudences = $this->jurisprudenceRepository->findJPByTitle($search);
                /** @var Jurisprudence $jurisprudence */
                foreach ($jurisprudences as $jurisprudence) {
                    $jpData = [
                        'title' => $jurisprudence->getName(),
                        'content' => $this->cleanContent($jurisprudence->getContent()),
                        'type' => 'jurisprudence',
                        'id' => $jurisprudence->getId()
                    ];

                    $data[$jurisprudence->getCreatedAt()->getTimestamp()] = $jpData;
                }
                $jurisprudences = $this->jurisprudenceRepository->findJurisprudencesByTag($search);
                /** @var Jurisprudence $jurisprudence */
                foreach ($jurisprudences as $jurisprudence) {
                    $jpData = [
                        'title' => $jurisprudence->getName(),
                        'content' => $this->cleanContent($jurisprudence->getContent()),
                        'type' => 'jurisprudence',
                        'id' => $jurisprudence->getId()
                    ];

                    // Vérifier que la JP n'est pas déjà dans le tableau
                    if (!array_key_exists($jurisprudence->getCreatedAt()->getTimestamp(), $data)) {
                        $data[$jurisprudence->getCreatedAt()->getTimestamp()] = $jpData;
                    }
                }
            }
        }

        ksort($data);

        return $this->render('search/search.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'dataContent' => $data,
            'categoryRequest' => $category,
            'search' => $search
        ]);
    }


    private function cleanContent(string $content)
    {
        $contentClean = strip_tags($content, '<br/><br>');
        $contentClean = str_replace("&rsquo;", "'", $contentClean);
        $contentClean = str_replace("&#39;", "'", $contentClean);

        return $contentClean;
    }
}
