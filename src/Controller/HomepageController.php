<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\VisitorCount;
use App\Repository\NewsCategoryRepository;
use App\Repository\NewsRepository;
use App\Repository\TutoRepository;
use App\Repository\VisitorCountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    private NewsCategoryRepository $newsCategoryRepository;
    private NewsRepository $newsRepository;
    private TutoRepository $tutoRepository;
    private VisitorCountRepository $visitorCountRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        NewsCategoryRepository $newsCategoryRepository,
        NewsRepository $newsRepository,
        TutoRepository $tutoRepository,
        VisitorCountRepository $visitorCountRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->newsRepository = $newsRepository;
        $this->tutoRepository = $tutoRepository;
        $this->visitorCountRepository = $visitorCountRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="default")
     */
    public function default(): Response
    {
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/accueil", name="homepage")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $categories = $this->newsCategoryRepository->findBy([
            'enabled' => true
        ], [
            'name' => 'asc'
        ]);

        // Stats total
        $totalCount = $this->visitorCountRepository->findOneBy(['type' => VisitorCount::TOTAL_VISITORS_TYPE]);
        if ($totalCount) {
            $totalCount->setCounter($totalCount->getCounter() + 1);
        } else {
            $totalCount = new VisitorCount();
            $totalCount->setType(VisitorCount::TOTAL_VISITORS_TYPE);
            $totalCount->setCounter(1);
            $this->entityManager->persist($totalCount);
        }
        $this->entityManager->flush();

        $categoryId = $request->query->get('category');
        if (null !== $categoryId) {
            $category = $this->newsCategoryRepository->find((int) $categoryId);
            $query = $this->newsRepository->findNewsActiveQueryByCategory($category);
        } else {
            $category = null;
            $query = $this->newsRepository->findNewsActiveQueryByCategory();
        }

        $data = $paginator->paginate($query, $request->query->get('page', 1), 6);

        $items = $data->getItems();
        $itemsWithoutTags = [];
        foreach ($items as $item) {
            $content = $item->getContent();
            $contentClean = strip_tags($content, '<br/><br>');
            $contentClean = str_replace("&rsquo;", "'", $contentClean);
            $contentClean = str_replace("&#39;", "'", $contentClean);
            $item->setContent($contentClean);
            $itemsWithoutTags[] = $item;
        }

        $data->setItems($itemsWithoutTags);
        
        return $this->render('homepage/homepage_default.html.twig', [
            'controller_name' => 'HomepageController',
            'active_menu' => 'homepage',
            'categories' => $categories,
            'categoryRequest' => $category,
            'news' => $data
        ]);
    }
}
