<?php

namespace App\Controller;

use App\Repository\TutoRepository;
use App\Services\VisitorCountManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LaBaseController extends AbstractController
{
    private TutoRepository $tutoRepository;
    private VisitorCountManager $visitorCountManager;

    public function __construct(TutoRepository $tutoRepository, VisitorCountManager $visitorCountManager)
    {
        $this->tutoRepository = $tutoRepository;
        $this->visitorCountManager = $visitorCountManager;
    }

    /**
     * @Route("/la-base", name="la_base")
     */
    public function laBase(Request $request, PaginatorInterface $paginator): Response
    {
        $tutoQuery = $this->tutoRepository->findActiveTutosQuery();
        $data = $paginator->paginate($tutoQuery, $request->query->get('page', 1), 4);

        // Remove striptags
        $items = $data->getItems();
        $itemsWithoutTags = [];
        foreach ($items as $item) {
            $content = $item->getContent();
            $contentClean = strip_tags($content, '<br/><br>');
            $contentClean = str_replace("&rsquo;", "'", $contentClean);
            $contentClean = str_replace("&#39;", "'", $contentClean);
            $item->setContent($contentClean);
            $itemsWithoutTags[] = $item;
        }

        $data->setItems($itemsWithoutTags);

        return $this->render('base/tutos_list.html.twig', [
            'controller_name' => 'LaBaseController',
            'active_menu' => 'base',
            'tutos' => $data
        ]);
    }

    /**
     * @Route("/lire-tuto/{tutoId}", name="tuto_read")
     */
    public function tutoRead(string $tutoId)
    {
        $tuto = $this->tutoRepository->find((int) $tutoId);
        if (null === $tuto) {
            throw new NotFoundHttpException();
        }

        // Count visitors
        $this->visitorCountManager->incrementStats($tuto);

        return $this->render('base/tuto_read.html.twig', [
            'controller_name' => 'LaBaseController',
            'active_menu' => 'base',
            'tuto' => $tuto
        ]);
    }
}
