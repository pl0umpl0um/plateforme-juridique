<?php

namespace App\Controller;

use App\Entity\Contribution;
use App\Entity\Jurisprudence;
use App\Entity\News;
use App\Entity\NewsCategory;
use App\Entity\Tuto;
use App\Form\Type\ContributionType;
use App\Repository\ContributionRepository;
use App\Repository\JurisprudenceCategoryRepository;
use App\Repository\NewsCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContributionController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private JurisprudenceCategoryRepository $jurisprudenceCategoryRepository;
    private ContributionRepository $contributionRepository;
    private NewsCategoryRepository $newsCategoryRepository;
    private AdminUrlGenerator $adminUrlGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        JurisprudenceCategoryRepository $jurisprudenceCategoryRepository,
        ContributionRepository $contributionRepository,
        AdminUrlGenerator $adminUrlGenerator,
        NewsCategoryRepository $newsCategoryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->jurisprudenceCategoryRepository = $jurisprudenceCategoryRepository;
        $this->contributionRepository = $contributionRepository;
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->newsCategoryRepository = $newsCategoryRepository;
    }

    /**
     * @Route("/contribution", name="contribution")
     */
    public function contribution(Request $request): Response
    {
        $contribution = new Contribution();
        $form = $this->createForm(ContributionType::class, $contribution);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contribution = $form->getData();

            // Persist contact message
            $this->entityManager->persist($contribution);
            $this->entityManager->flush();
            $this->addFlash('success', 'Contribution envoyée !');

            return $this->redirectToRoute('contribution');
        }

        return $this->render('contribute/new_contribution.html.twig', [
            'controller_name' => 'ContributionController',
            'active_menu' => 'contribution',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/contribution-admin-form", name="contribution_admin_form")
     */
    public function contributionAdminForm(Request $request)
    {
        $categories = $this->jurisprudenceCategoryRepository->findBy([
            'enabled' => true,
        ], [
            'name' => 'ASC'
        ]);

        $choices = [];
        foreach ($categories as $category) {
            $choices[$category->getName()] = $category->getId();
        }
        
        $form = $this->createFormBuilder()
            ->add('active', CheckboxType::class, [
                'label' => 'Afficher la jurisprudence sur le site',
                'required' => false
            ])
            ->add('category', ChoiceType::class, [
                'choices' => $choices,
                'label' => 'Choisir une catégorie',
            ])
            ->add('adress', TextType::class, [
                'label' => 'Ajouter une adresse',
                'required' => false
            ])
            ->add('justiceCourt', TextType::class, [
                'label' => 'Indiquer une cour de justice',
                'required' => false
            ])
            ->add('tags', TextType::class, [
                'label' => 'Ajouter des mots clés séparés par des virgules',
                'required' => false
            ])
            ->getForm();

        return $this->render('@EasyAdmin/Contribution/contribution_admin_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/contribution-admin-form-news", name="contribution_admin_form_news")
     */
    public function contributionAdminFormNews(Request $request)
    {
        $categories = $this->newsCategoryRepository->findBy([
            'enabled' => true,
        ], [
            'name' => 'ASC'
        ]);

        $choices = [];
        foreach ($categories as $category) {
            $choices[$category->getName()] = $category->getId();
        }

        $form = $this->createFormBuilder()
            ->add('active', CheckboxType::class, [
                'label' => "Afficher l'info sur le site",
                'required' => false
            ])
            ->add('category', ChoiceType::class, [
                'choices' => $choices,
                'label' => 'Choisir une catégorie',
                'required' => false
            ])
            ->getForm();

        return $this->render('@EasyAdmin/Contribution/contribution_admin_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/contribution-admin-form-tuto", name="contribution_admin_form_tuto")
     */
    public function contributionAdminFormTuto(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('active', CheckboxType::class, [
                'label' => "Afficher le tuto sur le site",
                'required' => false
            ])
            ->getForm();

        return $this->render('@EasyAdmin/Contribution/contribution_admin_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/contribution-admin-create-jurisprudence", name="contribution_to_jurisprudence")
     */
    public function createJurisprudenceFromContrib(Request $request)
    {
        // Get contrib
        $contribution = $this->contributionRepository->find((int) $request->get('contribution'));
        // Get category
        $category = $this->jurisprudenceCategoryRepository->find((int) $request->get('category'));
        // Build tags array from input
        $tagsArray = explode(',', $request->get('tags'));

        $jurisprudence = new Jurisprudence();
        if ('' !== $request->get('content')) {
            $jurisprudence->setContent($request->get('content'));
        } else {
            $jurisprudence->setContent($contribution->getContent());
        }
        $jurisprudence->setName($request->get('name'));
        $jurisprudence->setAdress($request->get('adress'));
        $jurisprudence->setJusticeCourt($request->get('justiceCourt'));
        $jurisprudence->setActive($request->get('active') == 'true' ? true: false);
        $jurisprudence->setDate(new \Datetime());
        $jurisprudence->setCategory($category);
        $jurisprudence->setTags($tagsArray);

        if ($request->get('active') == 'true') {
            $contribution->setPublished(true);
        }

        $contribution->setValidatedBy($this->getUser());

        $this->entityManager->persist($jurisprudence);
        $this->entityManager->flush();

        return new Response();
    }

    /**
     * @Route("/contribution-admin-create-news", name="contribution_to_news")
     */
    public function createNewsFromContrib(Request $request)
    {
        // Get contrib
        $contribution = $this->contributionRepository->find((int) $request->get('contribution'));
        // Get category
        $category = $this->newsCategoryRepository->find((int) $request->get('category'));

        $news = new News();
        if ('' !== $request->get('content')) {
            $news->setContent($request->get('content'));
        } else {
            $news->setContent($contribution->getContent());
        }
        $news->setTitle($request->get('name'));
        $news->setActive($request->get('active') == 'true' ? true: false);
        $news->setCategory($category);

        if ($request->get('active') == 'true') {
            $contribution->setPublished(true);
        }

        $contribution->setValidatedBy($this->getUser());

        $this->entityManager->persist($news);
        $this->entityManager->flush();

        return new Response();
    }

    /**
     * @Route("/contribution-admin-create-tuto", name="contribution_to_tuto")
     */
    public function createTutoFromContrib(Request $request)
    {
        // Get contrib
        $contribution = $this->contributionRepository->find((int) $request->get('contribution'));

        $tuto = new Tuto();
        if ('' !== $request->get('content')) {
            $tuto->setContent($request->get('content'));
        } else {
            $tuto->setContent($contribution->getContent());
        }
        $tuto->setTitle($request->get('name'));
        $tuto->setActive($request->get('active') == 'true' ? true: false);

        if ($request->get('active') == 'true') {
            $contribution->setPublished(true);
        }

        $contribution->setValidatedBy($this->getUser());

        // Flush tuto to get an id
        $this->entityManager->persist($tuto);
        $this->entityManager->flush();

        // Create the news associated
        $news = $this->createNewsForTuto($tuto);
        $this->entityManager->persist($news);
        // flush again for the actu
        $this->entityManager->flush();

        return new Response();
    }

    private function createNewsForTuto(Tuto $tuto): News
    {
        $tutoCategory = $this->newsCategoryRepository->findOneBy(['name' => 'Tuto']);
        if (null === $tutoCategory) {
            $tutoCategory = $this->newsCategoryRepository->findOneBy(['name' => 'Tutos']);
        }
        $news = new News();
        if ($tutoCategory) {
            $news->setCategory($tutoCategory);
        }
        $news->setTitle(sprintf('Un nouveau tuto est disponible : %s', $tuto->getTitle()));
        $news->setContent((string) $tuto->getId());
        if ($tuto->isActive()) {
            $news->setActive(true);
        }

        return $news;
    }
}
