<?php

namespace App\Repository;

use App\Entity\JurisprudenceCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JurisprudenceCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method JurisprudenceCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method JurisprudenceCategory[]    findAll()
 * @method JurisprudenceCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JurisprudenceCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JurisprudenceCategory::class);
    }

    public function getArrayJurisprudencesNoParents(): array
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.enabled = :enabled')
            ->andWhere('j.parent is NULL')
            ->setParameter('enabled', true)
            ->orderBy('j.name', 'ASC')
            ->getQuery()
            ->getArrayResult()
            ;
    }

    public function getArrayJurisprudencesChildrens(int $jpParentId = null): array
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.enabled = :enabled')
            ->andWhere('j.parent = :parent')
            ->setParameter('enabled', true)
            ->setParameter('parent', $jpParentId)
            ->orderBy('j.name', 'ASC')
            ->getQuery()
            ->getArrayResult()
            ;
    }
}
