<?php

namespace App\Repository;

use App\Entity\Jurisprudence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Jurisprudence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jurisprudence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jurisprudence[]    findAll()
 * @method Jurisprudence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JurisprudenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jurisprudence::class);
    }

    public function findJurisprudencesByTag(string $tag)
    {
        return $this->createQueryBuilder('j')
            ->where('j.tags LIKE :tags')
            ->setParameter('tags', '%"'.$tag.'"%')
            ->getQuery()
            ->getResult();
    }

    public function findJPByTitle(?string $title = null)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.active = :active')
            ->andWhere('j.name LIKE :name')
            ->setParameter('active', true)
            ->setParameter('name', '%'.$title.'%')
            ->orderBy('j.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
}
