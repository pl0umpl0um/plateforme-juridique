<?php

namespace App\Entity;

use App\Repository\VisitorCountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VisitorCountRepository::class)
 */
class VisitorCount
{
    const ARTICLE_READ_TYPE = 'article_read';
    const TOTAL_VISITORS_TYPE = 'total_type';
    const NONE_TYPE = 'none';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = '';

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $counter = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pageId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCounter(): ?int
    {
        return $this->counter;
    }

    public function setCounter(?int $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    public function getPageId(): ?int
    {
        return $this->pageId;
    }

    public function setPageId(?int $pageId): self
    {
        $this->pageId = $pageId;

        return $this;
    }

    public function __toString()
    {
        return $this->getCategory();
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
