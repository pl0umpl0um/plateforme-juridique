<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\News;
use App\Entity\NewsCategory;
use App\Entity\Tuto;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

;

class TutoEventSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            AfterEntityPersistedEvent::class => ['createNews'],
            AfterEntityUpdatedEvent::class => ['updateTuto'],
        ];
    }

    public function createNews(AfterEntityPersistedEvent $event)
    {
        $tuto = $event->getEntityInstance();
        if (!$tuto instanceof Tuto) {
            return;
        }
        $tutoCategory = $this->entityManager->getRepository(NewsCategory::class)->findOneBy(['name' => 'Tuto']);

        // Create a news for the last tuto created
        $news = new News();
        $news->setCategory($tutoCategory);
        $news->setTitle(sprintf('Un nouveau tuto est disponible : %s', $tuto->getTitle()));
        $news->setContent((string) $tuto->getId());
        if ($tuto->isActive()) {
            $news->setActive(true);
        }

        $this->entityManager->persist($news);
        $this->entityManager->flush();
    }

    public function updateTuto(AfterEntityUpdatedEvent $event)
    {
        $tuto = $event->getEntityInstance();
        if (!$tuto instanceof Tuto) {
            return;
        }

        // If tuto was'nt active but it is now
        if ($tuto->isActive()) {
            $newsLinked = $this->entityManager->getRepository(News::class)->findOneBy([
                'content' => (string) $tuto->getId()
            ]);

            if (null !== $newsLinked) {
                $newsLinked->setActive(true);
                $this->entityManager->persist($newsLinked);
                $this->entityManager->flush();
            }
        }
    }
}
