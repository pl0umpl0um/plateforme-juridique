<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221211125713 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contact CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE jurisprudence CHANGE category_id category_id INT DEFAULT NULL, CHANGE created_at created_at DATETIME DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL, CHANGE pdf pdf VARCHAR(255) DEFAULT NULL, CHANGE quality quality VARCHAR(20) DEFAULT NULL, CHANGE adress adress VARCHAR(255) DEFAULT NULL, CHANGE justice_court justice_court VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE jurisprudence_category CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE news CHANGE category_id category_id INT DEFAULT NULL, CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE news_category CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tuto CHANGE title title VARCHAR(255) NOT NULL, CHANGE content content LONGTEXT NOT NULL, CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE created_at created_at DATETIME DEFAULT NULL, CHANGE pseudo pseudo VARCHAR(180) DEFAULT NULL');
        $this->addSql('ALTER TABLE visitor_count CHANGE category category VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE jurisprudence_category CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE contact CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE pseudo pseudo VARCHAR(180) DEFAULT \'NULL\', CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE visitor_count CHANGE category category VARCHAR(255) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE jurisprudence CHANGE category_id category_id INT NOT NULL, CHANGE created_at created_at DATETIME DEFAULT \'NULL\', CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE pdf pdf VARCHAR(255) DEFAULT \'NULL\', CHANGE quality quality VARCHAR(20) DEFAULT \'NULL\', CHANGE adress adress VARCHAR(255) DEFAULT \'NULL\', CHANGE justice_court justice_court VARCHAR(255) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE tuto CHANGE title title VARCHAR(255) DEFAULT \'NULL\', CHANGE content content LONGTEXT DEFAULT NULL, CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE contribution CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE news CHANGE category_id category_id INT NOT NULL, CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE news_category CHANGE created_at created_at DATETIME DEFAULT \'NULL\'');
    }
}
